// Copyright (C) 2019-2020 Siodb GmbH. All rights reserved.
// Use of this source code is governed by a license that can be found
// in the LICENSE file.

package siodb

import (
	"bufio"
	"context"
	"database/sql"
	"encoding/hex"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"testing"
	"time"
)

// TLS connection (default)
var uri string = "siodbs://root@localhost:50000?identity_file=/home/siodb/.ssh/id_rsa"

// Plain text connection
// var uri string = "siodb://root@localhost:50000?identity_file=/home/siodb/.ssh/id_rsa"

// Local Unix socket connection
//var uri string = "siodbu://root@/run/siodb/siodb.socket?identity_file=/home/siodb/.ssh/id_rsa"

type testVars struct {
	db           *sql.DB
	databaseName string
	tableName    string
	tableColsDef string
	ctx          context.Context
	testing      *testing.T
}

func TestDatabase(t *testing.T) {

	db, err := sql.Open("siodb", uri)
	if err != nil {
		t.Fatalf("Error occured %s", err.Error())
	}
	defer db.Close()

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	if err := db.PingContext(ctx); err != nil {
		t.Fatalf("Error occured %s", err.Error())
	}

	var name string
	err = db.QueryRowContext(ctx, "select name from sys_databases where name = 'TEST'").Scan(&name)
	switch {
	case err == sql.ErrNoRows:
		if _, err := db.ExecContext(ctx, "CREATE DATABASE test"); err != nil {
			t.Fatalf("Error occured %s", err.Error())
		}
	case err != nil:
		t.Fatalf("Error occured %s", err.Error())
	case err == nil:
		break
	default:
		t.Fatalf("Error occured %s", err.Error())
	}
}

func TestAllDataTypes(t *testing.T) {

	db, err := sql.Open("siodb", uri)
	if err != nil {
		t.Fatalf("Error occured %s", err.Error())
	}
	defer db.Close()

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	if err := db.PingContext(ctx); err != nil {
		t.Fatalf("Error occured %s", err.Error())
	}

	var name string
	err = db.QueryRowContext(ctx, "select name from test.sys_tables where name = 'TABLEALLDATATYPES'").Scan(&name)
	switch {
	case err == sql.ErrNoRows:
		if _, err := db.ExecContext(ctx,
			`CREATE TABLE test.tablealldatatypes
						            (
						            	ctinyintmin  TINYINT,
						            	ctinyintmax  TINYINT,
			    		            	ctinyuint    TINYUINT,
						            	csmallintmin SMALLINT,
						            	csmallintmax SMALLINT,
						            	csmalluint   SMALLUINT,
						            	cintmin      INT,
						            	cintmax      INT,
						            	cuint        UINT,
						            	cbigintmin   BIGINT,
						            	cbigintmax   BIGINT,
						            	cbiguint     BIGUINT,
						            	cfloatmin    FLOAT,
						            	cfloatmax    FLOAT,
						            	cdoublemin   DOUBLE,
						            	cdoublemax   DOUBLE,
						            	ctext        TEXT,
						            	cts          TIMESTAMP
						            )
						            `); err != nil {
			t.Fatalf("Error occured: %s", err.Error())
		}
	case err != nil:
		t.Fatalf("Error occured: %s", err.Error())
	case err == nil:
		break
	default:
		t.Fatalf("Error occured: %s", err.Error())
	}

	if _, err := db.ExecContext(ctx,
		`INSERT INTO test.tablealldatatypes
	                            VALUES  ( -128,
	                            		  127,
	                            		  255,
	                            		  -32768,
	                            		  32767,
	                            		  65535,
	                            		  -2147483648,
	                            		  2147483647,
	                            		  4294967295,
	                            		  -9223372036854775808,
	                            		  9223372036854775807,
	                            		  18446744073709551615,
	                            		  222.222,
	                            		  222.222,
	                            		  222.222,
	                            		  222.222,
	                            		  '汉字',
	                            		  CURRENT_TIMESTAMP )`,
	); err != nil {
		log.Fatal("An Error occured: ", err)
	}

	rows, err := db.QueryContext(ctx, "SELECT trid, cbigintmin, ctext, cts FROM test.tablealldatatypes")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	var Trid uint64
	var AnyValue interface{}
	var StringValue string
	var TiemstampValue time.Time

	for rows.Next() {
		if err := rows.Scan(&Trid, &AnyValue, &StringValue, &TiemstampValue); err != nil {
			log.Fatal(err)
		}
		fmt.Println(fmt.Sprintf("Row: %v | %v | %v | %v ", Trid, AnyValue, StringValue, TiemstampValue))
	}
	if err := rows.Err(); err != nil {
		log.Fatal(err)
	}
}

func TestDataTypeIntegrity(t *testing.T) {

	db, err := sql.Open("siodb", uri)
	if err != nil {
		t.Fatalf("Error occured %s", err.Error())
	}
	defer db.Close()

	ctx, stop := context.WithCancel(context.Background())
	defer stop()

	// Check db connectivity
	if err := db.PingContext(ctx); err != nil {
		t.Fatalf("Error occured %s", err.Error())
	}

	// Configure test
	var tv testVars
	tv.db = db
	tv.testing = t
	tv.databaseName = "TEST"
	tv.tableName = "TESTALLDATATYPES"
	tv.tableColsDef = "name text, email text, subscription_date timestamp"
	tv.ctx = ctx

	// Launch tests
	tv.dataTypeIntegrity()

}

func (tv testVars) dataTypeIntegrity() {

	tv.testType("TINYINT", int8(-6))
	tv.testType("TINYUINT", uint8(89))
	tv.testType("SMALLINT", int16(-198))
	tv.testType("SMALLUINT", uint16(198))
	tv.testType("INT", int32(-93784))
	tv.testType("UINT", uint32(198381))
	tv.testType("BIGINT", int64(-9223372036854775808))
	tv.testType("BIGUINT", uint64(1844674407370955161))
	tv.testType("FLOAT", float32(2129213123131231233252129213123131231.2129213123131231/3))
	tv.testType("FLOAT", float32(-2129213123131231.2129213123131231233252129213123131231/9))
	tv.testType("DOUBLE", float64(3462.2121))
	tv.testType("DOUBLE", float64(27/11))
	tv.testType("TEXT", "汉字")
	tv.testType("TEXT", "mañana")
	tv.testType("TEXT", "Fußgängerübergänge")
	tv.testType("TEXT", "преподаватель")
	tv.testType("TEXT", "ωτορινολαρυγγολόγος")
	tv.testType("TEXT", "たいと")
	tv.testType("TEXT", "💓, 💑, 💵")
	tv.testBlob(string("/var/log/syslog"))
	tv.testTimeStamp(time.Now(), "2006-01-02 15:04:05.999999999")
	tv.testTimeStamp(time.Now(), "2006-01-02")

}

func (tv testVars) testTimeStamp(Date time.Time, tFormat string) {

	// Create the test table
	tableName := "T" + strconv.FormatInt(time.Now().UnixNano()/int64(time.Millisecond), 10)
	if _, err := tv.db.ExecContext(tv.ctx, "create table "+strings.ToUpper(tv.databaseName)+"."+tableName+" ( cTIMESTAMP TIMESTAMP )"); err != nil {
		log.Fatal("An Error occured: ", err)
	}

	var formattedDate string = Date.Format(tFormat)

	// Convert time to string
	if _, err := tv.db.ExecContext(tv.ctx, "insert into "+strings.ToUpper(tv.databaseName)+"."+tableName+" values ( '"+formattedDate+"' )"); err != nil {
		log.Fatal("An Error occured: ", err)
	}

	rows, err := tv.db.QueryContext(tv.ctx, "select cTIMESTAMP from "+strings.ToUpper(tv.databaseName)+"."+tableName+"")
	if err != nil {
		log.Fatal(err)
	}
	defer rows.Close()

	var ValueOut time.Time

	for rows.Next() {
		if err := rows.Scan(&ValueOut); err != nil {
			log.Fatal(err)
		}
	}
	if err := rows.Err(); err != nil {
		log.Fatal(err)
	}

	// Compare values
	if ValueOut.Format(tFormat) != Date.Format(tFormat) {
		fmt.Println(fmt.Sprintf("ERROR: test type TIMESTAMP            | Returned date (%s) is different from input date (%s).", ValueOut.Format(tFormat), Date.Format(tFormat)))
		tv.testing.Fatalf("ERROR")
	} else {
		fmt.Println(fmt.Sprintf("OK: test type TIMESTAMP               | Returned date (%s) is egual from input date (%s).", ValueOut.Format(tFormat), Date.Format(tFormat)))
	}

	return

}

func (tv testVars) testBlob(filePath string) {

	// Create the test table
	tableName := "T" + strconv.FormatInt(time.Now().UnixNano()/int64(time.Millisecond), 10)
	if _, err := tv.db.ExecContext(tv.ctx, "create table "+strings.ToUpper(tv.databaseName)+"."+tableName+" ( cBLOB BLOB )"); err != nil {
		log.Fatal("An Error occured: ", err)
	}

	// Open local file
	file, foperr := os.Open(filePath)
	if foperr != nil {
		tv.testing.Fatalf("ERROR: %s", foperr.Error())
		return
	}
	stats, statsErr := file.Stat()
	if statsErr != nil {
		tv.testing.Fatalf("ERROR: %s", statsErr.Error())
		return
	}

	// Load fie in memory
	var size int64 = stats.Size()
	bytes := make([]byte, size)
	bufr := bufio.NewReader(file)
	_, err := bufr.Read(bytes)
	if err != nil {
		tv.testing.Fatalf("ERROR: %s", err.Error())
		return
	}
	file.Close()

	if _, err := tv.db.ExecContext(tv.ctx, "insert into "+strings.ToUpper(tv.databaseName)+"."+tableName+" values ( '"+hex.EncodeToString(bytes)+"' )"); err != nil {
		tv.testing.Fatalf("ERROR: %s", err.Error())
	}

	rows, err := tv.db.QueryContext(tv.ctx, "select cBLOB from "+strings.ToUpper(tv.databaseName)+"."+tableName+"")
	if err != nil {
		tv.testing.Fatalf("ERROR: %s", err.Error())
	}
	defer rows.Close()

	var ValueOut []byte

	for rows.Next() {
		if err := rows.Scan(&ValueOut); err != nil {
			tv.testing.Fatalf("ERROR: %s", err.Error())
		}
	}
	if err := rows.Err(); err != nil {
		tv.testing.Fatalf("ERROR: %s", err.Error())
	}

	bytesOut, _ := hex.DecodeString(fmt.Sprintf("%s", ValueOut))
	f, err := os.Create("/tmp/" + filepath.Base(filePath))
	defer f.Close()
	n, err := f.Write(bytesOut)

	if len(bytesOut) != len(bytes) {
		fmt.Println(fmt.Sprintf("ERROR: test type BLOB                 | Returned size: %d | Wrotten bytes: %d.", len(bytesOut), n))
		tv.testing.Fatalf("ERROR")
	} else {
		fmt.Println(fmt.Sprintf("OK: test type BLOB                    | Returned size: %d | Wrotten bytes: %d.", len(bytesOut), n))
	}

	return

}

func (tv testVars) testType(Type string, ValueIn interface{}) {

	// Create the test table
	tableName := "T" + strconv.FormatInt(time.Now().UnixNano()/int64(time.Millisecond), 10)
	if _, err := tv.db.ExecContext(tv.ctx, "create table "+strings.ToUpper(tv.databaseName)+"."+tableName+" ( c"+Type+" "+Type+" )"); err != nil {
		tv.testing.Fatalf("ERROR: %s", err.Error())
	}

	switch Type {
	case "TEXT":
		if _, err := tv.db.ExecContext(tv.ctx, "insert into "+strings.ToUpper(tv.databaseName)+"."+tableName+" values ( '"+fmt.Sprintf("%v", ValueIn)+"' )"); err != nil {
			tv.testing.Fatalf("ERROR: %s", err.Error())
		}
	default:
		if _, err := tv.db.ExecContext(tv.ctx, "insert into "+strings.ToUpper(tv.databaseName)+"."+tableName+" values ( "+fmt.Sprintf("%v", ValueIn)+" )"); err != nil {
			tv.testing.Fatalf("ERROR: %s", err.Error())
		}
	}

	rows, err := tv.db.QueryContext(tv.ctx, "select c"+Type+" from "+strings.ToUpper(tv.databaseName)+"."+tableName+"")
	if err != nil {
		tv.testing.Fatalf("ERROR: %s", err.Error())
	}
	defer rows.Close()

	var ValueOut interface{}

	for rows.Next() {
		if err := rows.Scan(&ValueOut); err != nil {
			tv.testing.Fatalf("ERROR: %s", err.Error())
		}
	}
	if err := rows.Err(); err != nil {
		tv.testing.Fatalf("ERROR: %s", err.Error())
	}

	// Compare values
	if ValueOut != ValueIn {
		fmt.Println(fmt.Sprintf("ERROR: test type %-20s | ValueIn [%-8T]  %-20v == ValueOut [%-8T] %-25v", Type, ValueIn, ValueIn, ValueOut, ValueOut))
		tv.testing.Fatalf("ERROR")
	} else {
		fmt.Println(fmt.Sprintf("OK: test type %-20s    | ValueIn [%-8T]  %-20v == ValueOut [%-8T] %-25v", Type, ValueIn, ValueIn, ValueOut, ValueOut))
	}

}
